let questions=[
    {
        question:"Who is the founder and CEO of Talentsprint?",
        answers:[
            {text:"Santanu Paul",correct:true},
            {text:"TaTa",correct:false},
            {text:"Pawan Kumar",correct:false},
            {text:"Abhilash Misra",correct:false}
        ]
    },
    {
        question:"In which year Talentsprint was started in India?",
        answers:[
            {text:"2012",correct:false},
            {text:"2006",correct:false},
            {text:"2009",correct:true},
            {text:"2003",correct:false}
        ]
    },
    {
        question:"Which is the smallest container in the world?",
        answers:[
            {text:"Asia",correct:false},
            {text:"Australia",correct:true},
            {text:"Arctic",correct:false},
            {text:"Africa",correct:false}
        ]
    },
    {
        question:"Which is the largest desertr in the world?",
        answers:[
            {text:"Antarctica",correct:true},
            {text:"Gobi",correct:false},
            {text:"Sahara",correct:false},
            {text:"Kalahari",correct:false}
        ]
    }
];
let questionElement=document.getElementById("question");
let answerButton=document.getElementById("answer-buttons");
let previousButton=document.getElementById("pre-btn");
let nextButton=document.getElementById("next-btn");

let currentQuestionIndex=0;
let score=0;

function startQuiz()
{
    currentQuestionIndex=0;
    score=0;
    previousButton.innerHTML="Previous";
    nextButton.innerHTML="Next";
    showQuestion();
}
function showQuestion()
{
    resetState();
    let currentQuestion=questions[currentQuestionIndex];
    let questionNo=currentQuestionIndex + 1;
    questionElement.innerHTML=questionNo+"."+currentQuestion.question;
    currentQuestion.answers.forEach(answer=>{
        let button=document.createElement("button");
        button.innerHTML=answer.text;
        button.classList.add("btn");
        answerButton.appendChild(button);
        if(answer.correct){
            button.dataset.correct=answer.correct;
        }
        button.addEventListener("click",selectAnswer);
    });
}
function resetState()
{
    previousButton.style.display="block";
    nextButton.style.display="none";
    while(answerButton.firstChild){
        answerButton.removeChild(answerButton.firstChild);
    }
}
function selectAnswer(ans)
{
  let selectedBtn=ans.target;
  let Correct=selectedBtn.dataset.correct === "true";
  if(Correct){
    selectedBtn.classList.add("correct");
    score++;
    }
    else{
        selectedBtn.classList.add("incorrect");   
    }
    Array.from(answerButton.children).forEach(button=>{
        if(button.dataset.correct ==="true"){
            button.classList.add("correct");
        }
        button.disabled=true;
    });
    previousButton.style.display="flex";
    nextButton.style.display="block";
  }
  function showScore(){
    resetState();
    questionElement.innerHTML=`you scored ${score} out of ${questions.length}! `;
    previousButton.innerHTML="Back";
    previousButton.style.display="flex";
    nextButton.innerHTML="play Again";
    nextButton.style.display="block";
  }
  function handlepreviousButton(){
    currentQuestionIndex--;
    if(currentQuestionIndex < questions.length){
        showQuestion();
    }
    else{
        showScore();
    }
  } 
  previousButton.addEventListener("click",()=>{
if(currentQuestionIndex<questions.length)
{
    handlepreviousButton();
}
else{
    startQuiz();
}
  });
  function handleNextButton(){
    currentQuestionIndex++;
    if(currentQuestionIndex < questions.length){
        showQuestion();
    }
    else{
        showScore();
    }
  } 
 nextButton.addEventListener("click",()=>{
    if(currentQuestionIndex<questions.length){
        handleNextButton();
    }
    else{
        startQuiz() ;
    }
 });
 startQuiz() ;
