function fibonacciSeries(n1) {
    if (n1 <= 0) {
      return [];
    } else if (n1 === 1) {
      return [0];
    } else if (n1 === 2) {
      return [0, 1];
    }
  
    const series = [0, 1];
    for (let i = 2; i < n1; i++) {
      const nextNumber = series[i - 1] + series[i - 2];
      series.push(nextNumber);
    }
    return series;1
  }
  
  const n1 = 10; 
  const result1 = fibonacciSeries(n1);
  console.log(result1);
  
  //2nd question
  function calculateFactorial(n) {
    if (n === 0 || n === 1) {
      return 1;
    }
  
    let factorial = 1;
    for (let i = 2; i <= n; i++) {
      factorial *= i;
    }
    return factorial;
  }
  const n = 5; // Change this to the desired positive integer
  const result = calculateFactorial(n);
  console.log(`Factorial of ${n} is: ${result}`);  